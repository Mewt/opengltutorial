#pragma once

#include <GLFW\glfw3.h>
#include <glm.hpp>
#include "stdafx.h"

enum CameraMovement
{
	Forward,
	Backward,
	Left,
	Right,
	Upwards
};

class Camera
{
	const GLfloat MouseSensitivity = 0.25f;

	GLfloat		deltaTime = 0.0f;	// Time between current frame and last frame
	GLfloat		lastFrame = 0.0f;  	// Time of last frame

	glm::vec3	Pos;
	glm::vec3	Front;
	glm::vec3	Up;

	GLfloat		Pitch = 0;
	GLfloat		Yaw	  = -90.0f;

	GLfloat		fov	  = 45.0f;

public:

	Camera()
		: Pos(glm::vec3(0.0f, 0.0f,  3.0f)),
		  Front(glm::vec3(0.0f, 0.0f, -1.0f)),
		  Up(glm::vec3(0.0f, 1.0f,  0.0f))
	{
	}

	auto GetFov() const { return fov; }
	
	auto GetViewMatrix() const { return glm::lookAt(Pos, Pos + Front, Up); }

	auto GetPos() const { return Pos; }

	void SetPos(glm::vec3 pos) { Pos = pos; }

	auto GetFront() const { return Front; }

	void SetFront(glm::vec3 front) { Front = front; }

	void ProcessMovement(CameraMovement direction, GLfloat deltaTime)
	{
		const GLfloat Speed = 5.0f * deltaTime;

		if (direction == Forward)
		{
			Pos += Speed * Front;
			// Pos.y = 0.0f;
		}
	
		if (direction == Backward)
		{
			Pos -= Speed * Front;
			// Pos.y = 0.0f;
		}
	
		if (direction == Left)
		{
			Pos -= glm::normalize(glm::cross(Front, Up)) * Speed;
			// Pos.y = 0.0f;
		}
	
		if (direction == Right)
		{
			Pos += glm::normalize(glm::cross(Front, Up)) * Speed;
			// Pos.y = 0.0f;
		}
		
		if (direction == Upwards)
		{
			static bool goingUp = true;
			if (goingUp)
			{
				Pos.y += 0.005f;
			}
			else
			{
				Pos.y -= 0.005f;
			}

			if (Pos.y >= 1.0f)
				goingUp = false;

			if (Pos.y <= 0.0f)
				goingUp = true;
		}
	}

	void ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset)
	{
		xoffset *= MouseSensitivity;
		yoffset *= MouseSensitivity;

		Yaw += xoffset;
		Pitch += yoffset;

		if (Pitch > 89.0f)
			Pitch =  89.0f;
		if (Pitch < -89.0f)
			Pitch = -89.0f;

		glm::vec3 front;
		front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
		front.y = sin(glm::radians(Pitch));
		front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
		Front = glm::normalize(front);
	}

	void ProcessScroll(double yoffset)
	{
	  if (fov >= 1.0f && fov <= 45.0f)
	  {
  		fov -= yoffset;
	  }

	  if (fov <= 1.0f)
	  {
		  fov = 1.0f;
	  }

	  if (fov >= 45.0f)
	  {
		  fov = 45.0f;
	  }
	}

	glm::mat4 lookAt(glm::vec3 position, glm::vec3 target, glm::vec3 up) const
	{
		auto dir	= glm::normalize(position - target);	
		auto right	= glm::normalize(glm::cross(glm::normalize(up), dir));
		auto camUp  = glm::cross(dir, right);

		glm::mat4 translation;
		translation[3][0] = -position.x;
		translation[3][1] = -position.y;
		translation[3][2] = -position.z;

		glm::mat4 rotation;
		rotation[0][0] = right.x;
		rotation[1][0] = right.y;
		rotation[2][0] = right.z;
		rotation[0][1] = camUp.x;
		rotation[1][1] = camUp.y;
		rotation[2][1] = camUp.z;
		rotation[2][2] = dir.x;
		rotation[2][2] = dir.y;
		rotation[2][2] = dir.z;

		return rotation * translation;
	}
};
