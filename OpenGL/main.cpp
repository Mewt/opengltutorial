#include "stdafx.h"
#include "Input.h"
#include "GLHelperObjects.h"
#include "Initialization.h"

#include "Mesh.h"
#include "Model.h"

std::map<std::string, vertexArrayObject> arrayObjects;
std::vector<shaderProgram> shaders;
std::array<bool, 1024> keys;

std::map<std::string, Model> models;

GLfloat deltaTime = 0;
GLfloat lastFrame = 0;

void initModels()
{	
	/*texture diffuseContainer("textures\\container2.png", 0);
	texture specularContainer("textures\\container2_specular.png", 1);
	texture emissionContainer("textures\\container2_emission.png", 2);*/
	
	static std::vector<GLfloat> texturedCube = 
	{
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
		 0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

		 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
		 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f
	};

	arrayObjects["texturedCube"] = vertexArrayObject(texturedCube.size());
	arrayObjects["texturedCube"].Bind();

	vertexBufferObject vboCube(texturedCube, 5, true, false);

	arrayObjects["texturedCube"].UnBind();
	
	static std::vector<GLfloat> lightCube = 
	{
		// Positions           // Normals           // Texture Coords
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
		 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
	};

	arrayObjects["lightCube"] = vertexArrayObject(lightCube.size());
	arrayObjects["lightCube"].Bind();

	vertexBufferObject vboLightCube(lightCube, 8, true, true);

	arrayObjects["lightCube"].UnBind();

	models["nanosuit"] = Model("Textures\\NanoSuit\\nanosuit.obj");
}

void transform(shaderProgram shader)
{
	glm::mat4 trans;
	trans = glm::rotate(trans, glm::radians((GLfloat)(glfwGetTime() * 50.0f)), glm::vec3(0.0, 0.0, 1.0f));
	trans = glm::translate(trans, glm::vec3(0.5f, -0.5f, 0.0f));

	uniformVariable tex0(shader.Get(), "transform", trans);
}

glm::mat4 createModelMatrix()
{
	// rotate scene so that the x-axis is flat

	glm::mat4 ret;
	ret = glm::rotate(ret, glm::radians((GLfloat)glfwGetTime() * 50.0f), glm::vec3(0.5f, 1.0f, 0.0f));
	return ret;
}

glm::mat4 createProjectionMatrix()
{
	return glm::perspective(glm::radians(camera.GetFov()), (GLfloat)SCREEN_WIDTH / SCREEN_HEIGHT, 0.1f, 100.0f);
}

void drawTextured(const std::vector<shaderProgram>& shaders)
{
	static std::vector<glm::vec3> cubePositions = {
	  glm::vec3( 0.0f,  0.0f,  0.0f), 
	  glm::vec3( 2.0f,  5.0f, -15.0f), 
	  glm::vec3(-1.5f, -2.2f, -2.5f),  
	  glm::vec3(-3.8f, -2.0f, -12.3f),  
	  glm::vec3( 2.4f, -0.4f, -3.5f),  
	  glm::vec3(-1.7f,  3.0f, -7.5f),  
	  glm::vec3( 1.3f, -2.0f, -2.5f),  
	  glm::vec3( 1.5f,  2.0f, -2.5f), 
	  glm::vec3( 1.5f,  0.2f, -1.5f), 
	  glm::vec3(-1.3f,  1.0f, -1.5f)  
	};

	shaders[0].Use();

	uniformVariable view(shaders[0].Get(), "view", camera.GetViewMatrix());
	uniformVariable projection(shaders[0].Get(), "projection", createProjectionMatrix());

	uniformVariable tex0(shaders[0].Get(), "Texture1", (GLuint)0);
	uniformVariable tex1(shaders[0].Get(), "Texture2", (GLuint)1);
	uniformVariable mix(shaders[0].Get(), "mixAmount", mixAmount);

	auto cube = arrayObjects["texturedCube"];
	cube.Bind();
	
	for (auto i = 0; i < cubePositions.size(); ++i)
	{
		glm::mat4 model;
		model = glm::translate(model, cubePositions[i]);
/*
		if (i % 3 == 0 || i == 0)
		{
			model = glm::rotate(model, glm::radians((GLfloat)(glfwGetTime() * 50.0f)), glm::vec3(1.0f, 0.3f, 0.5f));
		}
		else
		{
			model = glm::rotate(model, glm::radians((GLfloat)(20.0f * i)), glm::vec3(1.0f, 0.3f, 0.5f));
		}*/

		uniformVariable modelUniform(shaders[0].Get(), "model", model);
		glDrawArrays(GL_TRIANGLES, 0, cube.GetNumVertices());
	}

	cube.UnBind();
}

void DrawLight(const shaderProgram* pShader, glm::vec3 lightPos, vertexArrayObject lightCube)
{
	shaders[1].Use();
	
	glm::mat4 model;
	model = glm::translate(model, lightPos);
	model = glm::scale(model, glm::vec3(0.2f)); // Make it a smaller cube

	uniformVariable mod(shaders[1].Get(), "model", model);
	uniformVariable view(shaders[1].Get(), "view", camera.GetViewMatrix());
	uniformVariable projection(shaders[1].Get(), "projection", createProjectionMatrix());

	glDrawArrays(GL_TRIANGLES, 0, lightCube.GetNumVertices());
}

void SetupDirectionalLight(const shaderProgram* pShader, glm::vec3 direction)
{
	uniformVariable			lightDir(pShader->Get(),		"dirLight.direction",	direction);
	static uniformVariable	lightAmbient(pShader->Get(),	"dirLight.ambient",		glm::vec3(0.1f));
	static uniformVariable	lightDiffuse(pShader->Get(),	"dirLight.diffuse",		glm::vec3(1.0f));
	static uniformVariable	lightSpecular(pShader->Get(),	"dirLight.specular",	glm::vec3(0.5f));
}

void SetupPointLights(const std::vector<shaderProgram>& Shaders, vertexArrayObject& lightCube, const std::vector<glm::vec3>& lightPos, const std::vector<glm::vec3>& lightColor)
{
	for (size_t i = 0; i < lightPos.size(); ++i)
	{
		DrawLight(&Shaders[1], lightPos[i], lightCube);

		shaders[0].Use();
		std::string varName("pointLight[" + std::to_string(i) + "].");
		uniformVariable	lightPosition(Shaders[0].Get(), varName + "position",	lightPos[i]);

		uniformVariable	lightAmbient(Shaders[0].Get(),	varName + "ambient",	glm::vec3(0.05f));
		uniformVariable	lightDiffuse(Shaders[0].Get(),	varName + "diffuse",	lightColor[i]); //glm::vec3(0.8f));
		uniformVariable	lightSpecular(Shaders[0].Get(),	varName + "specular",	glm::vec3(1.0f, 1.0f, 1.0f));

		uniformVariable	lightConstant(Shaders[0].Get(),	varName + "constant",	1.0f);
		uniformVariable	lightLinear(Shaders[0].Get(),	varName + "linear",		0.09f);
		uniformVariable	lightQuadratic(Shaders[0].Get(),varName + "quadratic",	0.032f);
	}
}

void SetupFlashlight(const shaderProgram* pShader, glm::vec3 lightPos, glm::vec3 lightDir)
{
	uniformVariable	lightDirection(pShader->Get(),	"spotLight.direction",	lightDir);
	uniformVariable	lightPosition(pShader->Get(),	"spotLight.position",	lightPos);
	uniformVariable	lightCutoff(pShader->Get(),		"spotLight.cutoff",		glm::cos(glm::radians(12.0f)));
	uniformVariable	lightOutCutoff(pShader->Get(),  "spotLight.outerCutoff",glm::cos(glm::radians(17.0f)));

	static uniformVariable	lightAmbient(pShader->Get(),   "spotLight.ambient",		glm::vec3(0.2f));
	static uniformVariable	lightDiffuse(pShader->Get(),   "spotLight.diffuse",		glm::vec3(0.5f));
	static uniformVariable	lightSpecular(pShader->Get(),  "spotLight.specular",	glm::vec3(1.0f, 1.0f, 1.0f));

	static uniformVariable	lightConstant(pShader->Get(),  "spotLight.constant",	1.0f);
	static uniformVariable	lightLinear(pShader->Get(),	   "spotLight.linear",		0.09f);
	static uniformVariable	lightQuadratic(pShader->Get(), "spotLight.quadratic",	0.032f);
}

void drawColoured(const std::vector<shaderProgram>& shaders)
{
	static std::vector<glm::vec3> cubePositions = {
	  glm::vec3(0.0f, 0.0f,  0.0f), 
	  glm::vec3( 2.0f,  5.0f, -15.0f), 
	  glm::vec3(-1.5f, -2.2f, -2.5f),  
	  glm::vec3(-3.8f, -2.0f, -12.3f),  
	  glm::vec3( 2.4f, -0.4f, -3.5f),  
	  glm::vec3(-1.7f,  3.0f, -7.5f),  
	  glm::vec3( 1.3f, -2.0f, -2.5f),  
	  glm::vec3( 1.5f,  2.0f, -2.5f), 
	  glm::vec3( 1.5f,  0.2f, -1.5f), 
	  glm::vec3(-1.3f,  1.0f, -1.5f) 
	};

	static std::vector<glm::vec3> pointLightPositions = {
		glm::vec3( 0.7f,  0.2f,  2.0f),
		glm::vec3( 2.3f, -3.3f, -4.0f),
		glm::vec3(-4.0f,  2.0f, -12.0f),
		glm::vec3( 0.0f,  0.0f, -3.0f)
	};  

	static std::vector<glm::vec3> pointLightColors = 
	{
		glm::vec3(1.0f),
		glm::vec3(1.0f),
		glm::vec3(1.0f),
		glm::vec3(1.0f),
	};

	auto lightCube = arrayObjects["lightCube"];
	lightCube.Bind();

	shaders[0].Use();
	
	SetupDirectionalLight(&shaders[0], glm::vec3(-0.2f, -1.0f, -0.3f));
	SetupPointLights(shaders, lightCube, pointLightPositions, pointLightColors);
	SetupFlashlight(&shaders[0], camera.GetPos(), camera.GetFront());

	for (int i = 0; i < cubePositions.size(); ++i)
	{
		const shaderProgram* pShader = &shaders[0];
		
		uniformVariable viewPos(pShader->Get(), "viewPos", camera.GetPos());

		static uniformVariable	tex0(pShader->Get(),			"material.texture_diffuse0", (GLuint)0);
		static uniformVariable	matSpecular(pShader->Get(),		"material.texture_specular0", (GLuint)1);
		static uniformVariable	matEmission(pShader->Get(),		"material.emission", (GLuint)2);
		static uniformVariable	matShininess(pShader->Get(),	"material.shininess", 64.0f);

		glm::mat4 model;
		model = glm::translate(model, cubePositions[i]);

		if (i % 3 == 0 || i == 0)
		{
			model = glm::rotate(model, glm::radians((GLfloat)(glfwGetTime() * 50.0f)), glm::vec3(1.0f, 0.3f, 0.5f));
		}
		else
		{
			model = glm::rotate(model, glm::radians((GLfloat)(20.0f * i)), glm::vec3(1.0f, 0.3f, 0.5f));
		}
	
		uniformVariable modelUniform(pShader->Get(), "model", model);
		uniformVariable view(pShader->Get(),		 "view", camera.GetViewMatrix());
		uniformVariable projection(pShader->Get(),	 "projection", createProjectionMatrix());

		glDrawArrays(GL_TRIANGLES, 0, lightCube.GetNumVertices());

	}

	lightCube.UnBind();
}

void drawModels(const shaderProgram& shader, float scale = 1.0f)
{
	shader.Use();
	
	SetupDirectionalLight(&shader, glm::vec3(-0.2f, -1.0f, -0.3f));
	// SetupPointLights(shaders, lightCube, pointLightPositions, pointLightColors);
	SetupFlashlight(&shader, camera.GetPos(), camera.GetFront());

	static std::array<glm::vec3, 2> modelPositions = { glm::vec3(0.0), glm::vec3(0.0f, 0.0f, -5.0f) };

	for (int i = 0; i < 2; ++i)
	{
		glm::mat4 model;
		model = glm::translate(model, modelPositions[i]);
		model = glm::scale(model, glm::vec3(scale));

		uniformVariable modelUniform(shader.Get(), "model", model);
		uniformVariable view(shader.Get(),		 "view", camera.GetViewMatrix());
		uniformVariable projection(shader.Get(),	 "projection", createProjectionMatrix());

		models["nanosuit"].Draw(shader);
	}
}

std::mutex mtx;
#include <Windows.h>

void ProcessJump()
{
	std::lock_guard<std::mutex> lck(mtx);
	do
	{
		camera.ProcessMovement(Upwards, deltaTime);
		Sleep(2);
	}
	while (camera.GetPos().y >= 0);
}

void ProcessMovement()
{
	GLfloat currentFrame = glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;  

	if (keys[GLFW_KEY_W])
		camera.ProcessMovement(Forward, deltaTime);
	
	if (keys[GLFW_KEY_S])
		camera.ProcessMovement(Backward, deltaTime);
	
	if (keys[GLFW_KEY_A])
		camera.ProcessMovement(Left, deltaTime);
	
	if (keys[GLFW_KEY_D])
		camera.ProcessMovement(Right, deltaTime);
/*
	if (keys[GLFW_KEY_SPACE])
	{
		keys[GLFW_KEY_SPACE] = false;

		std::thread jump(ProcessJump);
		jump.detach();
	}*/
}

void UpdateFPS(std::chrono::time_point<std::chrono::system_clock> startTime, GLFWwindow* window)
{
	static int numFrames = 0;
	++numFrames;
	
	auto runningSeconds = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - startTime);

	if (runningSeconds.count() < 1)
		return;

	std::string title("FPS: ");
	title.append(std::to_string(numFrames / runningSeconds.count()));
	glfwSetWindowTitle(window, title.c_str());
}

int main(int argc, char** argv)
{
	auto useColourShaders = false;
	if (argc == 3 && _stricmp(argv[1], "--shaders") == 0)
	{
		if (_stricmp(argv[2],"colour") == 0)
			useColourShaders = true;
	}

	try
	{
		auto window = initWindow();

		if (useColourShaders)
			shaders = GetColourShaders();
		else
			shaders = GetTextureShaders();

		initModels();

		int numFrames = 0;
		auto startTime = std::chrono::system_clock::now();

		while (!glfwWindowShouldClose(window))
		{
			UpdateFPS(startTime, window);

			glfwPollEvents();

			ProcessMovement();

			glClearColor((float)150/255, (float)150/255, (float)150/255, 1.0f);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			
			glEnable(GL_DEPTH_TEST);
			glStencilFunc(GL_ALWAYS, 1, 0xFF); // all fragments should update the stencil buffer
			glStencilMask(0xFF); // enable writing to the stencil buffer
			
			drawModels(shaders[0], 0.2f);

			//glStencilFunc(GL_NOTEQUAL, 1, 0xFF); // all fragments should update the stencil buffer
			//glStencilMask(0x00); // disable writing to the stencil buffer
			//glDisable(GL_DEPTH_TEST);
			//drawModels(shaders[2], 0.21f);

			glfwSwapBuffers(window);
		}
	}
	catch (std::runtime_error& e)
	{
		std::cerr << e.what() << std::endl;
		getchar();
	}
	
	glfwTerminate();

	return 0;
}
