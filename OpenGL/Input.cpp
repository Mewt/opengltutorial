#include "stdafx.h"
#include "Input.h"
#include "GLHelperObjects.h"

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (action == GLFW_PRESS)
	{
		keys[key] = true;
	}
	else if (action == GLFW_RELEASE)
	{
		keys[key] = false;
	}

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		// Cause the window to close when ESC is pressed
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	else if (key == GLFW_KEY_UP && action == GLFW_PRESS)
	{
		mixAmount += 0.1f;
		if (mixAmount >= 1.0f)
		{
			mixAmount = 1.0f;
		}
	}
	else if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
	{
		mixAmount -= 0.1f;
		if (mixAmount <= 0.0f)
		{
			mixAmount = 0.0f;
		}
	}
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
	static GLfloat lastX = 400;
	static GLfloat lastY = 300;
	static bool firstMouse = true;

    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left
    
    lastX = xpos;
    lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessScroll(yoffset);
}
