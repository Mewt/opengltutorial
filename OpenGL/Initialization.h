#pragma once

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

GLFWwindow* initWindow();

std::vector<shaderProgram> GetTextureShaders();
std::vector<shaderProgram> GetColourShaders();
