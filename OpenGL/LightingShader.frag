﻿#version 410 core

/////////////////////
// Structures
/////////////////////

struct DirectionalLight
{
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight
{
	vec3 position;
	vec3 color;

	float constant;
	float linear;
	float quadratic;
	
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct SpotLight
{
	vec3 position;
	vec3 direction;
	float cutoff;
	float outerCutoff;

	float constant;
	float linear;
	float quadratic;
	
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct Material
{
	sampler2D texture_diffuse0;
	sampler2D texture_diffuse1;
	sampler2D texture_diffuse2;
    sampler2D texture_specular0;
    sampler2D texture_specular1;
    sampler2D texture_specular2;
	sampler2D emission;
    float	  shininess;
};

/////////////////////
// Uniforms
/////////////////////

uniform vec3			 viewPos;
uniform DirectionalLight dirLight;
uniform Material		 material;

#define NR_POINT_LIGHTS	 4
uniform PointLight		 pointLight[NR_POINT_LIGHTS];
uniform SpotLight		 spotLight;

/////////////////////
// Inputs
/////////////////////

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;

/////////////////////
// Outputs
/////////////////////

out vec4 color;

/////////////////////
// Prototypes
/////////////////////

vec3 CalcDirLight(DirectionalLight light, vec3 normal, vec3 viewDir);

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);  

/////////////////////
// Functions
/////////////////////
 
void main()
{
	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(viewPos - FragPos);

	// Directional lighting

	vec3 result = CalcDirLight(dirLight, norm, viewDir);

	// Point lights

	// for (int i = 0; i < NR_POINT_LIGHTS; i++)
	// {
	// 	result += CalcPointLight(pointLight[i], norm, FragPos, viewDir);
	// }

	// Spot light
	result += CalcSpotLight(spotLight, norm, FragPos, viewDir);

	color = vec4(result, 1.0f);
}

vec3 CalcDirLight(DirectionalLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);

	// Diffuse
	float diff = max(dot(normal, lightDir), 0.0f);

	// Specluar
	
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);

	// Combine results

	vec3 ambient = light.ambient * (vec3(texture(material.texture_diffuse0, TexCoords)) + vec3(texture(material.texture_diffuse1, TexCoords)) + vec3(texture(material.texture_diffuse2, TexCoords)));
	vec3 diffuse = light.diffuse * diff * (vec3(texture(material.texture_diffuse0, TexCoords)) + vec3(texture(material.texture_diffuse1, TexCoords)) + vec3(texture(material.texture_diffuse2, TexCoords)));
	vec3 specular = light.specular * spec * (vec3(texture(material.texture_specular0, TexCoords)));

	return diffuse + ambient + specular;
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);

	// Diffuse
	float diff = max(dot(normal, lightDir), 0.0f);

	// Specular
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);
	
	// Attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	vec3 ambient = light.ambient * vec3(texture(material.texture_diffuse0, TexCoords));
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse0, TexCoords));
	vec3 specular = light.specular * spec * vec3(texture(material.texture_specular0, TexCoords));
	
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	return (ambient + diffuse + specular);
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);

	// Soft edges : I = θ − γ / ϵ

	// Theta
	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutoff - light.outerCutoff;
	float intensity = clamp((theta - light.outerCutoff) / epsilon, 0.0, 1.0);

	vec3 ambient = light.ambient * vec3(texture(material.texture_diffuse0, TexCoords));

	// Diffuse
	float diff = max(dot(normal, lightDir), 0.0f);
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse0, TexCoords));
	
	// Specular
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);
	vec3 specular = light.specular * spec * vec3(texture(material.texture_specular0, TexCoords));

	// Attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;

	return (ambient + diffuse + specular);
}
