#pragma once

struct Vertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
};

struct Texture
{
	GLuint		id;
	std::string type;
	aiString	path;
};

class shaderProgram
{
public:

	struct shaderInfo
	{
		GLuint type;
		std::string name;
	};

	shaderProgram(const std::vector<shaderInfo> shaders);

	GLuint	Get() const { return program; }
	void	Use() const { glUseProgram(program); }

private:

	GLuint Compile(GLenum type, const std::string& fileName);
	void   Load(const std::vector<shaderInfo>& shaderInfo);

	GLuint program;
};

extern std::vector<shaderProgram> shaders;

class vertexArrayObject
{
public:
	vertexArrayObject() { }
	vertexArrayObject(int numVertices) : numVertices(numVertices) { glGenVertexArrays(1, &VAO); }

	void Bind()					{ glBindVertexArray(VAO); }
	void UnBind()				{ glBindVertexArray(0); }

	int GetNumVertices() const	{ return numVertices; }

private:

	GLuint VAO;

	int numVertices;
};

class vertexBufferObject
{
public:

	vertexBufferObject(const std::vector<GLfloat>& vertices, int elemsPerVertex, bool hasTexture = false, bool hasNormals = false);
	vertexBufferObject() { }

	void DelayedInit(const std::vector<Vertex>& vertices, bool hasTexture = false, bool hasNormals = false);

private:

	GLuint VBO;
};

class elementBufferObject
{
public:
	elementBufferObject(const std::vector<GLuint>& indices);
	elementBufferObject() { }

	void DelayedInit(const std::vector<GLuint>& indices);

private:

	GLuint EBO;
};

class uniformVariable
{
public:
	uniformVariable(GLuint shaderProgram, const std::string& name, std::function<void (GLuint)>& setter);

	uniformVariable(GLuint shaderProgram, const std::string& name, float val);

	uniformVariable(GLuint shaderProgram, const std::string& name, GLuint val);
	
	uniformVariable(GLuint shaderProgram, const std::string& name, glm::vec3 val);

	uniformVariable(GLuint shaderProgram, const std::string& name, glm::mat4 val);

private:
};

class texture
{
public:
	texture() { }
	texture(const std::string& file, int textureId = 0);

	void Bind();
	void UnBind();

private:
	
	GLuint tex;
};