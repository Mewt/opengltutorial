#pragma once

class Model
{
public:

	std::vector<Texture> loadedTextures;

	Model() { };
	Model(const std::string& path);

	void Draw(const shaderProgram& shader);

private:

	std::vector<Mesh> meshes;
	std::string directory;

	void LoadModel(const std::string& path);

	void ProcessNode(aiNode* node, const aiScene* scene);
	Mesh ProcessMesh(aiMesh* mesh, const aiScene* scene);

	std::vector<Texture> LoadMaterialTextures(aiMaterial* mat, aiTextureType type, const std::string& typeName);
};
