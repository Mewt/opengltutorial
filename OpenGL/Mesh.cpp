#include "stdafx.h"
#include "GLHelperObjects.h"
#include "Mesh.h"

Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices, const std::vector<Texture>& textures)
	: vertices(vertices),
	  indices(indices),
	  textures(textures),
	  vao(vertices.size())
{
	SetupMesh();
}

void Mesh::SetupMesh()
{
	vao.Bind();
	vbo.DelayedInit(vertices, true, true);
	ebo.DelayedInit(indices);
	vao.UnBind();
}

void Mesh::Draw(const shaderProgram& shader)
{
	int diffuseN = 0;
	int specularN = 0;
	int number = 0;
	
	static uniformVariable	matShininess(shader.Get(), "material.shininess", 64.0f);

	for (size_t i = 0; i < textures.size(); ++i)
	{
		glActiveTexture(GL_TEXTURE0 + i);

        if (textures[i].type == "texture_diffuse")
		{
			number = diffuseN++;
		}
		else if (textures[i].type == "texture_specular")
		{
			number = specularN++;
		}

		std::string textureName("material." + textures[i].type + std::to_string(number));
		uniformVariable texture(shader.Get(), textureName, i);

		glBindTexture(GL_TEXTURE_2D, textures[i].id);
	}
	
	vao.Bind();
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	vao.UnBind();
}
