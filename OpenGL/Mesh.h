#pragma once

class Mesh
{
public:

	Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices, const std::vector<Texture>& textures);

	void Draw(const shaderProgram& shader);

private:

	std::vector<Vertex>		vertices;
	std::vector<GLuint>		indices;
	std::vector<Texture>	textures;

	vertexArrayObject	vao;
	vertexBufferObject	vbo;
	elementBufferObject	ebo;

	void SetupMesh();
};
