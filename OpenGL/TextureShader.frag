#version 410 core

in vec3 vertexColor;
in vec2 TexCoord;

out vec4 color;

uniform sampler2D Texture1;
uniform sampler2D Texture2;
uniform float mixAmount;

void main()
{
	color = mix(texture(Texture1, TexCoord), texture(Texture2, TexCoord), mixAmount);
}
