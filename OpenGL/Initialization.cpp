#include "stdafx.h"
#include "GLHelperObjects.h"
#include "Initialization.h"
#include "Input.h"

GLFWwindow* initWindow()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	auto window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Learn OpenGL", nullptr, nullptr);
	if (!window)
	{
		throw std::runtime_error("Failed to create window");
	}

	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		throw std::runtime_error("Failed to initialize glew");
	}

	int width;
	int height;
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);

	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	
	glfwSetKeyCallback(window, keyCallback);
	glfwSetCursorPosCallback(window, mouseCallback);
	glfwSetScrollCallback(window, scrollCallback);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE); 

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	return window;
}

std::vector<shaderProgram> GetShaders(const std::string& vertexShader, const std::string& fragmentShader)
{
	std::vector<shaderProgram> ret;

	shaderProgram::shaderInfo vert;
	vert.type = GL_VERTEX_SHADER;
	vert.name = vertexShader;

	shaderProgram::shaderInfo frag;
	frag.type = GL_FRAGMENT_SHADER;
	frag.name = fragmentShader;

	std::vector<shaderProgram::shaderInfo> normalShaderInfo = { vert, frag };

	ret.push_back(shaderProgram(normalShaderInfo));

	return ret;
}

std::vector<shaderProgram> GetTextureShaders()
{
	return GetShaders("TextureShader.vert", "TextureShader.frag");
}

std::vector<shaderProgram> GetColourShaders()
{
	auto ret = GetShaders("LightingShader.vert", "LightingShader.frag");
	auto light = GetShaders("Lamp.vert", "Lamp.frag");
	// auto stencil = GetShaders("Stencil.vert", "Stencil.frag");

	ret.push_back(light[0]);
	// ret.push_back(stencil[0]);

	return ret;
}
