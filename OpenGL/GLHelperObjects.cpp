#include "stdafx.h"
#include "GLHelperObjects.h"

shaderProgram::shaderProgram(const std::vector<shaderInfo> shaders)
{
	Load(shaders);
}

GLuint shaderProgram::Compile(GLenum type, const std::string& fileName)
{
	auto shader = glCreateShader(type);
	
	std::ifstream vert(fileName, std::ios::ate);
	if (!vert.is_open())
	{
		throw std::runtime_error("Failed to open shader");
	}

	vert.seekg(0, vert.end);
	int length = (int)vert.tellg();
	vert.seekg(0, vert.beg);

	std::vector<GLchar> file(length);
	vert.read(file.data(), length);

	const GLchar* data = file.data();

	glShaderSource(shader, 1, &data, nullptr);
	glCompileShader(shader);

	GLint success;
	GLchar infoLog[512] = {};
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	
	if (!success)
	{
		glGetShaderInfoLog(shader, 512, NULL, infoLog);

		std::string err("Failed to compile shader: ");
		err.append(infoLog);
		throw std::runtime_error(err);
	}

	return shader;
}

void shaderProgram::Load(const std::vector<shaderInfo>& shaderInfo)
{
	program = glCreateProgram();

	std::vector<GLuint> shaders;

	for (auto curr : shaderInfo)
	{
		auto shader = Compile(curr.type, curr.name.c_str());

		glAttachShader(program, shader);
	}
	
	glLinkProgram(program);

	GLint success;
	GLchar infoLog[512] = {};

	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(program, 512, NULL, infoLog);

		std::string err("Failed to link program: ");
		err.append(infoLog);
		throw std::runtime_error(err);
	}

	for (auto shader : shaders)
	{
		glDeleteShader(shader);
	}
}

vertexBufferObject::vertexBufferObject(const std::vector<GLfloat>& vertices, int elemsPerVertex, bool hasTexture, bool hasNormals)
{
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, elemsPerVertex * sizeof(vertices[0]), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	int offset = 0;

	if (hasNormals)
	{
		offset += 3;
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, elemsPerVertex * sizeof(vertices[0]), (GLvoid*)(offset * sizeof(vertices[0])));
		glEnableVertexAttribArray(1);
	}

	if (hasTexture)
	{
		offset += 3;
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, elemsPerVertex * sizeof(vertices[0]), (GLvoid*)(offset * sizeof(vertices[0])));
		glEnableVertexAttribArray(2);
	}

}

void vertexBufferObject::DelayedInit(const std::vector<Vertex>& vertices, bool hasTexture, bool hasNormals)
{
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * vertices.size(), vertices.data(), GL_STATIC_DRAW);
	
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	if (hasNormals)
	{
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(offsetof(Vertex, Normal)));
	}

	if (hasTexture)
	{
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)(offsetof(Vertex, TexCoords)));
	}
}

elementBufferObject::elementBufferObject(const std::vector<GLuint>& indices)
{
	DelayedInit(indices);
}

void elementBufferObject::DelayedInit(const std::vector<GLuint>& indices)
{
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), indices.data(), GL_STATIC_DRAW);
}

uniformVariable::uniformVariable(GLuint shaderProgram, const std::string& name, std::function<void (GLuint)>& setter)
{
	auto location = glGetUniformLocation(shaderProgram, name.c_str());
	glUseProgram(shaderProgram);

	setter(location);
}

uniformVariable::uniformVariable(GLuint shaderProgram, const std::string& name, float val)
{
	auto location = glGetUniformLocation(shaderProgram, name.c_str());
	glUseProgram(shaderProgram);
	glUniform1f(location, val);
}

uniformVariable::uniformVariable(GLuint shaderProgram, const std::string& name, GLuint val)
{
	auto location = glGetUniformLocation(shaderProgram, name.c_str());
	glUseProgram(shaderProgram);
	glUniform1i(location, val);
}

uniformVariable::uniformVariable(GLuint shaderProgram, const std::string& name, glm::vec3 val)
{
	auto location = glGetUniformLocation(shaderProgram, name.c_str());
	glUseProgram(shaderProgram);
	glUniform3f(location, val.x, val.y, val.z);
}

uniformVariable::uniformVariable(GLuint shaderProgram, const std::string& name, glm::mat4 val)
{
	auto location = glGetUniformLocation(shaderProgram, name.c_str());
	glUseProgram(shaderProgram);
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(val));
}

texture::texture(const std::string& file, int textureId)
{
	glGenTextures(1, &tex);

	glActiveTexture(GL_TEXTURE0 + textureId);

	Bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	
	int width, height;
	auto image = SOIL_load_image(file.c_str(), &width, &height, 0, SOIL_LOAD_RGB);

	if (!image)
		throw std::runtime_error("failed to load texture");

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(tex);

	SOIL_free_image_data(image);
	UnBind();
}

void texture::Bind()
{
	glBindTexture(GL_TEXTURE_2D, tex);
}

void texture::UnBind()
{
}