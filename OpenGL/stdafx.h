#pragma once

#define GLEW_STATIC
#include <gl/glew.h>

#include <glfw/glfw3.h>

#include <soil.h>

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <memory>
#include <functional>
#include <array>
#include <mutex>
#include <string>
#include <chrono>

#include "Camera.h"

extern float mixAmount;

extern std::array<bool, 1024> keys;

extern GLfloat mouseLastX;
extern GLfloat mouseLastY;

extern Camera camera;
